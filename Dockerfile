ARG BINARY_NAME=rathole
ARG BINARY_PATH=/bin/${BINARY_NAME}
ARG TARGETPLATFORM=arm

FROM docker.io/alpine:latest as binaryfetcher

WORKDIR /data

ARG BINARY_PATH
ARG BINARY_NAME

RUN apk add --no-cache wget unzip --no-cache

RUN if [ "$TARGETPLATFORM" = "arm" ]; then wget "https://github.com/rapiz1/rathole/releases/latest/download/rathole-arm-unknown-linux-musleabihf.zip" -O /data/${BINARY_NAME}.zip; else wget "https://github.com/rapiz1/rathole/releases/latest/download/rathole-x86_64-unknown-linux-musl.zip" -O /data/${BINARY_NAME}.zip; fi

RUN unzip /data/${BINARY_NAME}.zip
RUN chmod +x /data/${BINARY_NAME}

FROM docker.io/alpine:latest as app
ARG BINARY_NAME
WORKDIR /app
COPY --from=binaryfetcher /data/${BINARY_NAME} .
USER 1000:1000
CMD ["/app/rathole", "./config.toml"]

# https://admantium.medium.com/docker-building-images-for-multiple-architectures-4f142f6dda71
# docker buildx build -f ./Dockerfile --platform linux/arm64,linux/amd64 -t name:tag --push .
